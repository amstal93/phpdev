# Docker PHP/Apache Production Images

Based on `PHP:apache` for running PHP in Production.

Rewrite and SSL enabled.

## Usage

`docker run -d -p <HTTP_PORT>:80 -p <HTTPS_PORT>:443 --hostname <HOSTNAME> --name <CONTAINER_NAME> --volume $PWD:/var/www/html/ maxtpower/php`
